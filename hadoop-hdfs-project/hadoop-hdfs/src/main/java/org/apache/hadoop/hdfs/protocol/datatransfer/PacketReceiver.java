/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.hadoop.hdfs.protocol.datatransfer;
import java.util.zip.*;
import java.io.Closeable;
import java.io.*;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.classification.InterfaceAudience;
import org.apache.hadoop.util.DirectBufferPool;
import org.apache.hadoop.io.IOUtils;

import com.google.common.base.Preconditions;
import com.google.common.primitives.Ints;

/**
 * Class to handle reading packets one-at-a-time from the wire.
 * These packets are used both for reading and writing data to/from
 * DataNodes.
 */
@InterfaceAudience.Private
public class PacketReceiver implements Closeable {

  /**
   * The max size of any single packet. This prevents OOMEs when
   * invalid data is sent.
   */
  private static final int MAX_PACKET_SIZE = 16 * 1024 * 1024;
  public int otherLen=0;
  public int dataLen=0;
  public int trueValueRead=0;
  static final Log LOG = LogFactory.getLog(PacketReceiver.class);
  
  private static final DirectBufferPool bufferPool = new DirectBufferPool();
  private final boolean useDirectBuffers;

  /**
   * The entirety of the most recently read packet.
   * The first PKT_LENGTHS_LEN bytes of this buffer are the
   * length prefixes.
   */
  private ByteBuffer curPacketBuf = null;
  private boolean comp = false; 
  /**
   * A slice of {@link #curPacketBuf} which contains just the checksums.
   */
  private ByteBuffer curChecksumSlice = null;
  
  /**
   * A slice of {@link #curPacketBuf} which contains just the data.
   */
  private ByteBuffer curDataSlice = null;

  /**
   * The packet header of the most recently read packet.
   */
  private PacketHeader curHeader;
  
  public PacketReceiver(boolean useDirectBuffers) {
    this.useDirectBuffers = useDirectBuffers;
    reallocPacketBuf(PacketHeader.PKT_LENGTHS_LEN);
  }

  public PacketHeader getHeader() {
    return curHeader;
  }

  public ByteBuffer getDataSlice() {
    return curDataSlice;
  }
  
  public ByteBuffer getChecksumSlice() {
    return curChecksumSlice;
  }
  public int getDataSize(){
	return trueValueRead - otherLen;
  }
  /**
   * Reads all of the data for the next packet into the appropriate buffers.
   * 
   * The data slice and checksum slice members will be set to point to the
   * user data and corresponding checksums. The header will be parsed and
   * set.
   */
  public void receiveNextPacket(ReadableByteChannel in) throws IOException {
    doRead(in, null);
  }

  /**
   * @see #receiveNextPacket(ReadableByteChannel)
   */
  public void receiveNextPacket(InputStream in) throws IOException {
    doRead(null, in);
  }

  private void doRead(ReadableByteChannel ch, InputStream in)//we read from the socked
      throws IOException {
    // Each packet looks like:
    //   PLEN    HLEN      HEADER     CHECKSUMS  DATA
    //   32-bit  16-bit   <protobuf>  <variable length>
    //
    // PLEN:      Payload length
    //            = length(PLEN) + length(CHECKSUMS) + length(DATA)
    //            This length includes its own encoded length in
    //            the sum for historical reasons.
    //
    // HLEN:      Header length
    //            = length(HEADER)
    //
    // HEADER:    the actual packet header fields, encoded in protobuf
    // CHECKSUMS: the crcs for the data chunk. May be missing if
    //            checksums were not requested
    // DATA       the actual block data
    Preconditions.checkState(curHeader == null || !curHeader.isLastPacketInBlock());
 //   in = new GZIPInputStream(in);//modified
    curPacketBuf.clear();//->buffer is clear
    curPacketBuf.limit(PacketHeader.PKT_LENGTHS_LEN);//index of the first index which should not be read/length prefixed
     trueValueRead = doReadFully(ch, in, curPacketBuf,0);
 	//doReadFully(ch, in, curPacketBuf);
     //trueValueRead=0;
    curPacketBuf.flip();//limit is set to current pos and current pos is set to 0
    int payloadLen = curPacketBuf.getInt();//since cur pos goes back to 0 we get the payload length
    
    if (payloadLen < Ints.BYTES) {
      // The "payload length" includes its own length. Therefore it
      // should never be less than 4 bytes
      throw new IOException("Invalid payload length " +
          payloadLen);
    }
    int dataPlusChecksumLen = payloadLen - Ints.BYTES;
    int headerLen = curPacketBuf.getShort();
    if (headerLen < 0) {
      throw new IOException("Invalid header length " + headerLen);
    }
    
    if (true) {//modified
      LOG.trace("readNextPacket: dataPlusChecksumLen = " + dataPlusChecksumLen +
          " headerLen = " + headerLen);
    }
    LOG.info("Amount Read1 : " + trueValueRead);
    // Sanity check the buffer size so we don't allocate too much memory
    // and OOME.
    int totalLen = payloadLen + headerLen;
    if (totalLen < 0 || totalLen > MAX_PACKET_SIZE) {
      throw new IOException("Incorrect value for packet payload size: " +
                            payloadLen);
    }

    // Make sure we have space for the whole packet, and
    // read it.
    reallocPacketBuf(PacketHeader.PKT_LENGTHS_LEN +
        dataPlusChecksumLen + headerLen);
    curPacketBuf.clear();
    curPacketBuf.position(PacketHeader.PKT_LENGTHS_LEN);
    curPacketBuf.limit(PacketHeader.PKT_LENGTHS_LEN +
        dataPlusChecksumLen + headerLen);
//     doReadFully(ch, in, curPacketBuf); 
    trueValueRead = doReadFully(ch,in,curPacketBuf,0);
    //doReadFully(ch,in,curPacketBuf);
	//if (trueValueRead != dataPlusChecksumLen + headerLen) comp = true;//this will only occur for the second two datanodes
    curPacketBuf.flip();
    curPacketBuf.position(PacketHeader.PKT_LENGTHS_LEN);
	LOG.info("True Value Read: " + trueValueRead);
    // Extract the header from the front of the buffer (after the length prefixes)
    byte[] headerBuf = new byte[headerLen];
    curPacketBuf.get(headerBuf);
    if (curHeader == null) {
      curHeader = new PacketHeader();
    }
    curHeader.setFieldsFromData(dataPlusChecksumLen, headerBuf);
    
    // Compute the sub-slices of the packet
    int checksumLen = dataPlusChecksumLen - curHeader.getDataLen();
    if (checksumLen < 0) {
      throw new IOException("Invalid packet: data length in packet header " + 
          "exceeds data length received. dataPlusChecksumLen=" +
          dataPlusChecksumLen + " header: " + curHeader); 
    }
    dataLen = curHeader.getDataLen();
    otherLen = headerLen + checksumLen + PacketHeader.PKT_LENGTHS_LEN;
    reslicePacket(headerLen, checksumLen, curHeader.getDataLen(),trueValueRead);//modified
  }
  public boolean getComp(){
	return comp;
	}
  /**
   * Rewrite the last-read packet on the wire to the given output stream.
   */
  public void mirrorPacketTo(DataOutputStream mirrorOut,byte[] trueData,int trueLength) throws IOException {///add integer value for length of new datalen buffer
    Preconditions.checkState(!useDirectBuffers,
        "Currently only supported for non-direct buffers");
	LOG.info("Block is compressed? " + comp);
//	if (compressed == true){
	//if (comp == false){
	if (comp==true){
          LOG.info("FLY10");
 /*   mirrorOut.write(curPacketBuf.array(),//underlying array, current offset,remaining offset
        curPacketBuf.arrayOffset(),
        curPacketBuf.remaining());*/
	//mirrorOut.write(curPacketBuf.array(),curPacketBuf.arrayOffset(),otherLen);//write checksum and packet headers as normal
//	mirrorOut.write(trueData,0,trueLength); //writes the compressed data
 //     mirrorOut.write(curPacketBuf.array(),curPacketBuf.arrayOffset(),trueValueRead);
 	mirrorOut.write(curPacketBuf.array(),curPacketBuf.arrayOffset(),curPacketBuf.remaining());//simply write the compressed data amount the whole packet

	}
      else{
	 int check = 0;
	if (trueData != null){
	//if (false){
	LOG.info("FLY11");
	LOG.info("TRUE DATA: " + trueData.length);
	LOG.info(" REMAINING : " + curPacketBuf.remaining());
	curPacketBuf.position(otherLen);
			LOG.info("OFF: " + curPacketBuf.arrayOffset() + " Remaining:  " + curPacketBuf.remaining() + " Position: " + curPacketBuf.position() + " Otherlen: " + otherLen);
	LOG.info(" REMAINING : " + curPacketBuf.remaining());
	if (curPacketBuf.remaining() > trueLength){
	check = 1;
	curPacketBuf.put(trueData,0,trueLength);
	}
			LOG.info("OFF: " + curPacketBuf.arrayOffset() + " Remaining:  " + curPacketBuf.remaining() + " Position: " + curPacketBuf.position() + " Otherlen: " + otherLen);

	curPacketBuf.position(0);
	if (check ==1) curPacketBuf.limit(otherLen + trueLength);

			LOG.info("OFF: " + curPacketBuf.arrayOffset() + " Remaining:  " + curPacketBuf.remaining() + " Position: " + curPacketBuf.position());

//	curPacketBuf.limit(otherLen+trueLength);
	mirrorOut.write(curPacketBuf.array(),curPacketBuf.arrayOffset(),curPacketBuf.remaining());//simply write the compressed data amount the whole packet
/*	mirrorOut.write(curPacketBuf.array(),curPacketBuf.arrayOffset(),otherLen);
    	GZIPOutputStream mirrorOutC = new GZIPOutputStream(mirrorOut);
	mirrorOutC.write(curPacketBuf.array(),otherLen,dataLen);//compress the data */
	//mirrorOut.write(curPacketBuf.array(),curPacketBuf.arrayOffset(),otherLen);d
//	mirrorOut.write(trueData,0,trueLength);
//	mirrorOut.write(trueData);d
// 	mirrorOut.write(curPacketBuf.array(),curPacketBuf.arrayOffset(),trueValueRead);//simply write the compressed data amount the whole packet

	}
	else {
			LOG.info("OFF: " + curPacketBuf.arrayOffset() + " Remaining:  " + curPacketBuf.remaining() + " Position: " + curPacketBuf.position());
			mirrorOut.write(curPacketBuf.array(),curPacketBuf.arrayOffset(),curPacketBuf.remaining());//simply write the compressed data amount the whole packet
	}

  }
}

  
  private static void doReadFully(ReadableByteChannel ch, InputStream in,//modified to int from void
      ByteBuffer buf) throws IOException {
//	int value = 0;
    if (ch != null) {
      readChannelFully(ch, buf);
    } else {
      Preconditions.checkState(!buf.isDirect(),
          "Must not use direct buffers with InputStream API");
        IOUtils.readFully(in, buf.array(),//modified
          buf.arrayOffset() + buf.position(),
          buf.remaining());//this is the number of elements from arrayoffset to limit
      buf.position(buf.position() + buf.remaining());
    //  return value;
    }
//	return value;
  }
  private static int doReadFully(ReadableByteChannel ch, InputStream in,//modified to int from void
      ByteBuffer buf, int check) throws IOException {
	int value = 0;
    if (ch != null) {
      readChannelFully(ch, buf);
    } else {
      Preconditions.checkState(!buf.isDirect(),
          "Must not use direct buffers with InputStream API");
       value = IOUtils.readFully(in, buf.array(),//modified
          buf.arrayOffset() + buf.position(),
          buf.remaining(),0);//this is the number of elements from arrayoffset to limit
      buf.position(buf.position() + buf.remaining());
    //  return value;
    }
	return value;
  }

  private void reslicePacket(
      int headerLen, int checksumsLen, int dataLen, int trueDataLen) {
    // Packet structure (refer to doRead() for details):
    //   PLEN    HLEN      HEADER     CHECKSUMS  DATA
    //   32-bit  16-bit   <protobuf>  <variable length>
    //   |--- lenThroughHeader ----|
    //   |----------- lenThroughChecksums   ----|
    //   |------------------- lenThroughData    ------| 
    int lenThroughHeader = PacketHeader.PKT_LENGTHS_LEN + headerLen;
    int lenThroughChecksums = lenThroughHeader + checksumsLen;
    int lenThroughData = lenThroughChecksums + dataLen;
    LOG.info("Len through data" + lenThroughData);
    if (lenThroughData != trueDataLen) comp=true;
    assert dataLen >= 0 : "invalid datalen: " + dataLen;
    assert curPacketBuf.position() == lenThroughHeader;
    assert curPacketBuf.limit() == lenThroughData :
      "headerLen= " + headerLen + " clen=" + checksumsLen + " dlen=" + dataLen +
      " rem=" + curPacketBuf.remaining();
	
    // Slice the checksums.
    curPacketBuf.position(lenThroughHeader);
    curPacketBuf.limit(lenThroughChecksums);
    curChecksumSlice = curPacketBuf.slice();

    // Slice the data.
    curPacketBuf.position(lenThroughChecksums);
 //   curPacketBuf.limit(lenThroughData);
     curPacketBuf.limit(trueDataLen);
    curDataSlice = curPacketBuf.slice();
    
    // Reset buffer to point to the entirety of the packet (including
    // length prefixes)
    curPacketBuf.position(0);
   // curPacketBuf.limit(lenThroughData);
    curPacketBuf.limit(trueDataLen);
  }

  
  private static void readChannelFully(ReadableByteChannel ch, ByteBuffer buf)
      throws IOException {
    while (buf.remaining() > 0) {
      int n = ch.read(buf);
      if (n < 0) {
        throw new IOException("Premature EOF reading from " + ch);
      }
    }
  }
  
  private void reallocPacketBuf(int atLeastCapacity) {
    // Realloc the buffer if this packet is longer than the previous
    // one.
    if (curPacketBuf == null ||
        curPacketBuf.capacity() < atLeastCapacity) {
      ByteBuffer newBuf;
      if (useDirectBuffers) {
        newBuf = bufferPool.getBuffer(atLeastCapacity);
      } else {
        newBuf = ByteBuffer.allocate(atLeastCapacity);
      }
      // If reallocing an existing buffer, copy the old packet length
      // prefixes over
      if (curPacketBuf != null) {
        curPacketBuf.flip();
        newBuf.put(curPacketBuf);
      }
      
      returnPacketBufToPool();
      curPacketBuf = newBuf;
    }
  }
  
  private void returnPacketBufToPool() {
    if (curPacketBuf != null && curPacketBuf.isDirect()) {
      bufferPool.returnBuffer(curPacketBuf);
      curPacketBuf = null;
    }
  }

  @Override // Closeable
  public void close() {
    returnPacketBufToPool();
  }
  
  @Override
  protected void finalize() throws Throwable {
    try {
      // just in case it didn't get closed, we
      // may as well still try to return the buffer
      returnPacketBufToPool();
    } finally {
      super.finalize();
    }
  }
}
